"""Tests for Q Compile kit transpiler passes"""
import math
import numpy as np
import scipy

from qiskit import QuantumCircuit
from qiskit.converters import circuit_to_dag, dag_to_circuit
from qiskit.circuit.library import QFT
from qiskit.compiler import transpile
from qiskit.quantum_info import Operator

from bqskit import Circuit
from bqskit.passes import LEAPSynthesisPass

from qcompilekit.passes import (
    BQSKitTransformationPass,
    BQSKitQSearchPass,
    BQSKitQFastPass,
)


def _trace_distance(circuit1, circuit2):
    """Return the trace distance of the two input circuits."""
    op1, op2 = Operator(circuit1), Operator(circuit2)
    return 0.5 * np.trace(scipy.linalg.sqrtm(np.conj(op1 - op2).T.dot(op1 - op2))).real


def test_base_pass_class():
    """Test ``BQSKitTransformationPass`` base pass class."""
    qs = BQSKitTransformationPass(synthesis_pass=LEAPSynthesisPass)
    assert qs.synthesis_pass == LEAPSynthesisPass
    assert qs.config == dict()
    assert qs.dag_to_circuit(circuit_to_dag(QuantumCircuit(1))) == Circuit(1)

    dag = qs.circuit_to_dag(Circuit(1))
    assert len(dag.qubits) == 1
    assert len(dag.count_ops()) == 0
    assert dag.depth() == 0


def test_qsearch_pass_returns_simple_circuit():
    """Test that ``BQSKitQSearchPass`` work with the I-gate."""
    circuit = QuantumCircuit(1)
    circuit.i(0)
    qs = BQSKitQSearchPass()
    decomposed_circuit = qs(circuit)
    assert decomposed_circuit.count_ops()["u3"] == 1


def test_qsearch_trivial_decomposition():
    """Test that the a circuit that can be represented exactly is represented exactly."""
    circuit = QuantumCircuit(1)
    circuit.u(math.pi/2, 2 * math.pi, math.pi, 0)

    qs = BQSKitQSearchPass()

    dag = circuit_to_dag(circuit)
    decomposed_dag = qs.run(dag)
    decomposed_circuit = dag_to_circuit(decomposed_dag)
    assert decomposed_circuit.count_ops()["u3"] == 1


def test_qsearch_approximation_on_qft():
    """Test the QSEARCH on the QFT circuit."""
    qft = QFT(3)
    transpiled = transpile(qft, basis_gates=["u3", "cx"], optimization_level=1)

    qs = BQSKitQSearchPass()

    decomposed_circuit = qs(transpiled)
    assert _trace_distance(transpiled, decomposed_circuit) < 15

