"""Tests for Q Compile kit plugins"""
import qiskit
from bqskit import Circuit
from bqskit.ir.gates import CNOTGate, ISwapGate, U3Gate
from bqskit.ir.lang import get_language
from bqskit.passes.search import (
    DijkstraHeuristic,
    GreedyHeuristic,
    SimpleLayerGenerator,
)
from qiskit import ClassicalRegister, QuantumCircuit, QuantumRegister
from qiskit.compiler import transpile

TOFFOLI_UNITARY = [
    [1, 0, 0, 0, 0, 0, 0, 0],
    [0, 1, 0, 0, 0, 0, 0, 0],
    [0, 0, 1, 0, 0, 0, 0, 0],
    [0, 0, 0, 1, 0, 0, 0, 0],
    [0, 0, 0, 0, 1, 0, 0, 0],
    [0, 0, 0, 0, 0, 1, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 1],
    [0, 0, 0, 0, 0, 0, 1, 0],
]


def test_plugins_installed():
    """Test checks that plugins were installed and registered"""
    plugins = qiskit.transpiler.passes.synthesis.plugin.unitary_synthesis_plugin_names()
    assert "qsearch" in plugins
    assert "qfast" in plugins


def test_qsearch_run():
    """Test for Bell State with QSearch compiler"""
    q = QuantumRegister(2, "q")
    c = ClassicalRegister(2, "c")
    circuit = QuantumCircuit(q, c)
    circuit.h(q[0])  # Hadamard gate
    circuit.cx(q[0], q[1])  # CNOT gate
    circuit.measure(q, c)  # Qubit Measurment
    result = transpile(circuit, unitary_synthesis_method="qsearch")
    assert (
        result.qasm()
        == 'OPENQASM 2.0;\ninclude "qelib1.inc";\nqreg q[2];\ncreg c[2];\nh q[0];\ncx q[0],q[1];\nmeasure q[0] -> c[0];\nmeasure q[1] -> c[1];\n'
    )


def test_qfast_run():
    """Test for Bell State with QSearch compiler"""
    q = QuantumRegister(2, "q")
    c = ClassicalRegister(2, "c")
    circuit = QuantumCircuit(q, c)
    circuit.h(q[0])  # Hadamard gate
    circuit.cx(q[0], q[1])  # CNOT gate
    circuit.measure(q, c)  # Qubit Measurment
    result = transpile(circuit, unitary_synthesis_method="qfast")
    assert (
        result.qasm()
        == 'OPENQASM 2.0;\ninclude "qelib1.inc";\nqreg q[2];\ncreg c[2];\nh q[0];\ncx q[0],q[1];\nmeasure q[0] -> c[0];\nmeasure q[1] -> c[1];\n'
    )


def test_qsearch_toffoli():
    """Test for circuit depth and operations with Toffoli 3-q"""
    circuit = QuantumCircuit(3)
    circuit.unitary(TOFFOLI_UNITARY, [0, 1, 2])
    result = transpile(circuits=circuit, unitary_synthesis_method="qsearch")
    language = get_language("qasm")
    synthesized_circuit = language.decode(result.qasm())
    assert synthesized_circuit.depth == 13
    assert synthesized_circuit.num_operations == 21
    assert synthesized_circuit.count(CNOTGate()) == 6
    assert synthesized_circuit.count(U3Gate()) == 15


def test_qsearch_accuracy():
    """Test for QSEARCH accuracy"""
    config = {
        "success_threshold": 1e-1,
    }

    circuit = QuantumCircuit(3)
    circuit.unitary(TOFFOLI_UNITARY, [0, 1, 2])
    result = transpile(
        circuits=circuit,
        unitary_synthesis_method="qsearch",
        unitary_synthesis_plugin_config=config,
    )
    language = get_language("qasm")
    synthesized_circuit = language.decode(result.qasm())
    out_utry = synthesized_circuit.get_unitary()
    assert synthesized_circuit.count(CNOTGate()) == 4
    assert out_utry.get_distance_from(TOFFOLI_UNITARY) < 0.39


def test_qsearch_gatest_and_layer():
    """Test for QSEARCH gatesets and layer Generation"""
    layer_gen = SimpleLayerGenerator(
        two_qudit_gate=ISwapGate(), single_qudit_gate_1=U3Gate()
    )
    config = {
        "layer_generator": layer_gen,
    }

    circuit = QuantumCircuit(3)
    circuit.unitary(TOFFOLI_UNITARY, [0, 1, 2])
    result = transpile(
        circuits=circuit,
        unitary_synthesis_method="qsearch",
        unitary_synthesis_plugin_config=config,
    )
    language = get_language("qasm")
    synthesized_circuit = language.decode(result.qasm())
    assert synthesized_circuit.gate_set == {ISwapGate(), U3Gate()}


def test_qsearch_heuristics():
    """Test QSEARCH heuristics algorithm"""
    circuit = QuantumCircuit(3)
    circuit.unitary(TOFFOLI_UNITARY, [0, 1, 2])
    result = transpile(
        circuits=circuit,
        unitary_synthesis_method="qsearch",
        unitary_synthesis_plugin_config={"heuristic_function": GreedyHeuristic()},
    )
    language = get_language("qasm")
    synthesized_circuit = language.decode(result.qasm())
    out_utry = synthesized_circuit.get_unitary()
    assert out_utry.get_distance_from(TOFFOLI_UNITARY) < 0.39

    result = transpile(
        circuits=circuit,
        unitary_synthesis_method="qsearch",
        unitary_synthesis_plugin_config={"heuristic_function": DijkstraHeuristic()},
    )
    language = get_language("qasm")
    synthesized_circuit = language.decode(result.qasm())
    out_utry = synthesized_circuit.get_unitary()
    assert out_utry.get_distance_from(TOFFOLI_UNITARY) < 0.39
