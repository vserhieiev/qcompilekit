"""
Wrappers for BQSKit to make it compatible with Qiskit transpiler passes.
"""
from functools import partial

from qiskit.converters import dag_to_circuit
from qiskit.transpiler.basepasses import TransformationPass
from qiskit.dagcircuit import DAGCircuit
from bqskit import Circuit
from bqskit.compiler.compiler import Compiler
from bqskit.compiler.task import CompilationTask
from bqskit.ir.lang import get_language
from bqskit.passes import (
    QSearchSynthesisPass,
    QFASTDecompositionPass,
)
from bqskit.passes.synthesis.synthesis import SynthesisPass
from qiskit import QuantumCircuit
from qiskit.converters import circuit_to_dag


class BQSKitTransformationPass(TransformationPass):
    """The BQSKit base transpiler pass class for Qiskit."""

    def __init__(
        self,
        synthesis_pass: SynthesisPass,
        config: dict = {},
    ) -> None:
        """
        Args:
            synthesis_pass: The BQSKit synthesis pass class.
            config: The BQSKit options dict.
        """
        super().__init__()
        self.synthesis_pass = synthesis_pass
        self.config = config

    def dag_to_circuit(self, dag: DAGCircuit) -> Circuit:
        """Convert the DAG to the BQSKit circuit"""
        language = get_language("qasm")
        return language.decode(dag_to_circuit(dag).qasm())

    def circuit_to_dag(self, circuit: Circuit) -> DAGCircuit:
        """Convert the BQSKit circuit to the DAG"""
        return circuit_to_dag(QuantumCircuit.from_qasm_str(circuit.to("qasm")))

    def run(self, dag: DAGCircuit) -> DAGCircuit:
        """Run the ``BQSKitPass`` pass on `dag`.

        Args:
            dag: The input dag.

        Returns:
            Output dag with gates synthesized.
        """
        circuit = self.dag_to_circuit(dag=dag)
        task = CompilationTask(circuit, [self.synthesis_pass(**self.config)])
        with Compiler() as compiler:
            transpiled = compiler.compile(task)

        return self.circuit_to_dag(transpiled)


BQSKitQSearchPass = partial(BQSKitTransformationPass, synthesis_pass=QSearchSynthesisPass)
BQSKitQFastPass = partial(BQSKitTransformationPass, synthesis_pass=QFASTDecompositionPass)
