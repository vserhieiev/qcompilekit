"""This module contains the version information for QCompileKit."""
from __future__ import annotations

__version_info__ = ("1", "0", "0", "a1")
__version__ = ".".join(__version_info__[:3]) + "".join(__version_info__[3:])
