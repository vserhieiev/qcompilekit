"""
Qiskit plugin to use BQSKit's Search-based Synthesis (QSEARCH).
BQSKit Search-based Synthesis Guide:
https://github.com/BQSKit/bqskit/blob/main/examples/tutorials/Search%20Synthesis.ipynb
"""
from bqskit import Circuit
from bqskit.compiler.compiler import Compiler
from bqskit.compiler.task import CompilationTask
from bqskit.passes import QSearchSynthesisPass
from qiskit import QuantumCircuit
from qiskit.converters import circuit_to_dag

from .base import BQSKitBaseSynthesisPlugin


class BQSKitQSearchSynthesis(BQSKitBaseSynthesisPlugin):
    """
    BQSKit plugin class for QSEARCH.
    """

    def run(self, unitary, **options):
        """
        Performs QSEARCH compilation task.
        """
        config = options["config"] or {}
        circuit = Circuit.from_unitary(unitary)
        task = CompilationTask(circuit, [QSearchSynthesisPass(**config)])
        with Compiler() as compiler:
            compiled_circuit = compiler.compile(task)

        return circuit_to_dag(QuantumCircuit.from_qasm_str(compiled_circuit.to("qasm")))
