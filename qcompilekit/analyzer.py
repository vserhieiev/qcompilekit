"""
Circuit Analyzer class.
Based on Arline Benchmarks: https://github.com/ArlineQ/arline_benchmarks
"""
import os

from arline_quantum.estimators import Estimator
from arline_quantum.gates import __gates_by_names__


def analyse(anls_func):
    """Decorator to marks analyzer functions."""

    def anls_and_save(self, target, gate_chain):
        anls_name = anls_func.__name__
        if os.environ.get("DEBUG"):
            print(anls_name, anls_func(self))
        self.report.update(anls_func(self, target, gate_chain))

    anls_and_save.is_analyse_function = True
    return anls_and_save


class BasicAnalyser:
    """Basic Gate Chain Analyser Class

    Returns: following relevant metrics:
        * Gate count by gate type
        * Circuit depth
        * Number of populated qubits (qubits involved in calculation)
    """

    def __init__(
        self, verbose=False, cost_cfg={"class": "IbmCostFunction", "args": {}}
    ):
        self.report = {}
        self.anls_list = None
        self.verbose = verbose
        self.cost_model = Estimator.from_config(cost_cfg)

    def run_all(self, target, gate_chain):
        for f_name in self.available_anls():
            getattr(self, f_name)(target, gate_chain)
        return self.report

    def available_anls(self):
        self.report = {}
        return [
            a
            for a in dir(self)
            if callable(getattr(self, a))
            and hasattr(getattr(self, a), "is_analyse_function")
        ]

    def run_selected(self, target, gate_chain):
        for f_name in self.anls_list:
            getattr(self, f_name)(target, gate_chain)
        return self.report

    @analyse
    def depth(self, target, gate_chain):
        return {"Depth": gate_chain.get_depth()}

    @analyse
    def total_gate_count(self, target, gate_chain):
        return {"Total Gate Count": gate_chain.get_num_gates()}

    @analyse
    def gate_count_by_num_qubits(self, target, gate_chain):
        """Gate count by the number of qubits the gate acts on"""
        n_qubits = set()
        for g in gate_chain.quantum_hardware.gate_set.gate_list:
            n_qubits.add(g.num_qubits)
        # Add Single Qubit and Two Qubit Gates by default
        n_qubits.update({1, 2})

        r = {n: 0 for n in range(1, max(n_qubits) + 1)}
        for gc in gate_chain.chain:
            r[gc._gate.num_qubits] += 1
        gcm = {}
        for n, v in r.items():
            if n == 1:
                gcm["Single-Qubit Gate Count"] = v
            elif n == 2:
                gcm["Two-Qubit Gate Count"] = v
            else:
                gcm["{n}-Qubit Gate Count"] = v
        return gcm

    @analyse
    def gate_depth_by_num_qubits(self, target, gate_chain):
        """Gate depth by the number of qubits the gate acts on"""
        n_qubits = set()
        for g in gate_chain.quantum_hardware.gate_set.gate_list:
            n_qubits.add(g.num_qubits)
        # Add Single Qubit and Two Qubit Gates by default
        n_qubits.update({1, 2})

        r = {n: 0 for n in range(1, max(n_qubits) + 1)}

        for n in range(1, max(n_qubits) + 1):
            gate_names = [
                g._gate.name for g in gate_chain.chain if g._gate.num_qubits == n
            ]
            r[n] = gate_chain.get_depth_by_gate_type(gate_names)
        gcm = {}
        for n, v in r.items():
            if n == 1:
                gcm["Single-Qubit Gate Depth"] = v
            elif n == 2:
                gcm["Two-Qubit Gate Depth"] = v
            else:
                gcm["{n}-Qubit Gate Depth"] = v
        return gcm

    @analyse
    def gate_count_by_type(self, target, gate_chain):
        gates_from_chain = gate_chain.get_gate_count()
        return {
            "Count of {} Gates".format(gate_name): gates_from_chain[gate_name]
            if gate_name in gates_from_chain
            else 0
            for gate_name in __gates_by_names__.keys()
        }

    @analyse
    def gate_depth_by_type(self, target, gate_chain):
        gates_from_chain = gate_chain.get_gate_count()
        return {
            "Depth of {} Gates".format(gate_name): gate_chain.get_depth_by_gate_type(
                [gate_name]
            )
            if gate_name in gates_from_chain
            else 0
            for gate_name in __gates_by_names__.keys()
        }

    @analyse
    def gate_chain_cost_function(self, target, gate_chain):
        return {"Circuit Cost Function": self.cost_model.calculate_cost(gate_chain)}

    @analyse
    def num_populated_qubit(self, target, gate_chain):
        populated_qubits = 0
        for i in range(gate_chain.quantum_hardware.num_qubits):
            if gate_chain.get_num_gates_by_qubits(i) != 0:
                populated_qubits += 1
        return {"Number of Populated Qubits": populated_qubits}

    @analyse
    def connectivity_check(self, target, gate_chain):
        """Check Connectivity Violations"""
        violations = gate_chain.check_connectivity()
        return {
            "Connectivity Satisfied": (
                violations if self.verbose else (len(violations) == 0)
            )
        }

    @analyse
    def gate_set_check(self, target, gate_chain):
        """Check Gate Set Violations"""
        violations = gate_chain.check_gate_set()
        return {
            "Gate Set Satisfied": (
                violations if self.verbose else (len(violations) == 0)
            )
        }

    @analyse
    def qubit_number_check(self, target, gate_chain):
        """Check Qubit Number Violations"""
        violations = gate_chain.check_qubit_number()
        return {
            "Qubit Number Satisfied": (
                violations if self.verbose else (len(violations) == 0)
            )
        }
