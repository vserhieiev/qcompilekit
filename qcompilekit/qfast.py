"""
Qiskit plugin to use BQSKit's Searchless Synthesis (QFAST).
BQSKit Searchless Synthesis Guide:
https://github.com/BQSKit/bqskit/blob/main/examples/tutorials/Searchless%20Synthesis.ipynb
"""
from bqskit import Circuit
from bqskit.compiler.compiler import Compiler
from bqskit.compiler.task import CompilationTask
from bqskit.passes import QFASTDecompositionPass
from qiskit import QuantumCircuit
from qiskit.converters import circuit_to_dag

from .base import BQSKitBaseSynthesisPlugin


class BQSKitQFastSynthesis(BQSKitBaseSynthesisPlugin):
    """
    BQSKit plugin class for QFAST.
    """

    def run(self, unitary, **options):
        """Performs QSEARCH compilation task"""
        config = options["config"] or {}
        circuit = Circuit.from_unitary(unitary)
        task = CompilationTask(circuit, [QFASTDecompositionPass(**config)])
        with Compiler() as compiler:
            compiled_circuit = compiler.compile(task)

        return circuit_to_dag(QuantumCircuit.from_qasm_str(compiled_circuit.to("qasm")))
