"""
BQSKit base plugin class for  Qiskit.
"""
from qiskit.transpiler.passes.synthesis import plugin


class BQSKitBaseSynthesisPlugin(plugin.UnitarySynthesisPlugin):
    """
    BQSKit base plugin class.
    """

    @property
    def supports_basis_gates(self):
        return True

    @property
    def supports_coupling_map(self):
        return True

    @property
    def supports_natural_direction(self):
        return False

    @property
    def supports_pulse_optimize(self):
        return False

    @property
    def supports_gate_lengths(self):
        return False

    @property
    def supports_gate_errors(self):
        return False

    @property
    def supports_gate_lengths_by_qubit(self):
        return False

    @property
    def supports_gate_errors_by_qubit(self):
        return False

    @property
    def min_qubits(self):
        return 1

    @property
    def max_qubits(self):
        return 1024

    @property
    def supported_bases(self):
        return None

    def run(self, unitary, **options):
        raise NotImplemented
