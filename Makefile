define USAGE
Super awesome hand-crafted build system ⚙️

Commands:
	install		Install this Python package and dependencies
	test		Run linters and tests.
	style		Run black and isort.
	benchmark	Run benchmarks.
	lima		Run benchmarks with Lima device.
	multipass	Run benchmarks with multiple passes.
endef

export USAGE
help:
	@echo "$$USAGE"

install:
	pip3 install .

test:
	python -m pytest -s .

style:
	black . && isort .

benchmark:
	python ./benchmarks/run.py

lima:
	python ./benchmarks/lima.py

multipass:
	python ./benchmarks/multipass.py
