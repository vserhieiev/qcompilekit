"""
Benchmarks runner: Multiple transpiler passes using Qiskit and BQSKit.
Compares Qiskit and BQSKit results.
"""
from qiskit import QuantumCircuit, transpile
from qiskit.circuit.random import random_circuit

from qcompilekit.passes import BQSKitQSearchPass, BQSKitQFastPass


def report(label: str, circuit: QuantumCircuit) -> None:
    print(label, circuit.depth())
    print(label, circuit.count_ops())


def main():
    """
    Benchmarks entry point. Loop over random circuits and produce report.
    """
    qubits = 3
    basis_gates = ["id", "u3", "cx"]
    qs = BQSKitQSearchPass()
    qf = BQSKitQFastPass()

    for depth in [5, 10, 20, 30, 40, 50]:
        print("==============================================================")
        circuit = random_circuit(qubits, depth)
        transpiled1 = transpile(circuit, basis_gates=basis_gates, optimization_level=1)
        transpiled2 = transpile(circuit, basis_gates=basis_gates, optimization_level=2)
        transpiled3 = transpile(circuit, basis_gates=basis_gates, optimization_level=3)

        benchmarks = {
            "circuit": circuit,
            "transpiled1": transpiled1,
            "transpiled2": transpiled2,
            "transpiled3": transpiled3,

            "qsearch1": qs(transpiled1),
            "qsearch2": qs(transpiled2),
            "qsearch3": qs(transpiled3),

            # "qfast1": qf(transpiled1),
            # "qfast2": qf(transpiled2),
            # "qfast3": qf(transpiled3),
        }

        for k, v in benchmarks.items():
            report(k, v)


if __name__ == "__main__":
    main()
