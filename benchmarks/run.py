"""
Benchmarks runner. Analyzes circuits compilation/transpilation using
Qiskit, Cirq, pytket and BQSKit libraries.

Returns JSON-formatted report.
"""
import json
from os import path

import cirq
from arline_quantum.gate_chain.gate_chain import GateChain
from bqskit.ir.gates import ISwapGate, U3Gate
from bqskit.passes.search import (
    DijkstraHeuristic,
    GreedyHeuristic,
    SimpleLayerGenerator,
)
from cirq.contrib.qasm_import import circuit_from_qasm
from cirq_ionq import ionq_gateset

# from pytket.passes import SynthesiseIBM  # removed in latest pytket
from pytket.qasm import circuit_from_qasm as circuit_from_qasm_pytket
from qiskit import QuantumCircuit
from qiskit.compiler import transpile

from qcompilekit.analyzer import BasicAnalyser

QASM_FILES = [
    "C17_204.qasm",
    "mini-alu_167.qasm",
    "one-two-three-v0_97.qasm",
    "rd53_135.qasm",
    "sym9_146.qasm",
]

QASM_DIR = path.join(path.dirname(path.realpath(__file__)), "circuits")


def analyze(circuit, compiler="qiskit", target=None):
    """
    Analyze circuits from different compilers.

    Args:
        circuit: Quantum program/circuit.
        compiler: Framework (qiskit, cirq, pytket).

    Returns:
        str: Analyzer report.
    """
    analyzer = BasicAnalyser()
    return analyzer.run_all(
        target=target, gate_chain=GateChain.convert_from(circuit, compiler)
    )


def _do_benchmark_qiskit(qasm_path):
    """
    Qiskit circuits.

    Args:
        qasm_path: Path to quantum program/circuit file.

    Returns:
        dict(str): Analyzer reports.
    """
    circuit = QuantumCircuit.from_qasm_file(qasm_path)

    return {
        "qiskit": analyze(circuit),
        "qiskit_optimized1": analyze(transpile(circuit, optimization_level=1)),
        "qiskit_optimized2": analyze(transpile(circuit, optimization_level=2)),
        "qiskit_optimized3": analyze(transpile(circuit, optimization_level=3)),
    }


def _do_benchmark_cirq(qasm_path):
    """
    Cirq circuits.

    Args:
        qasm_path: Path to quantum program/circuit file.

    Returns:
        dict(str): Analyzer reports.
    """
    with open(qasm_path) as f:
        circuit = circuit_from_qasm(f.read())

    return {
        "cirq": analyze(circuit, compiler="cirq"),
        "cirq_optimized": analyze(
            cirq.optimize_for_target_gateset(
                circuit, gateset=ionq_gateset.IonQTargetGateset()
            ),
            compiler="cirq",
        ),
    }


def _do_benchmark_pytket(qasm_path):
    """
    Pytket circuits.

    Args:
        qasm_path: Path to quantum program/circuit file.

    Returns:
        dict(str): Analyzer reports.
    """
    # circuit_pytket = circuit_from_qasm_pytket(qasm_path)
    # TODO: removed in recent pytket, neet to find other solution
    # circuit_pytket_optimized = SynthesiseIBM().apply(circuit_tket)

    return {
        "pytket": analyze(circuit_from_qasm_pytket(qasm_path), compiler="pytket"),
    }


def _do_benchmark_bqskit(qasm_path):
    """
    BQSKit circuits.

    Args:
        qasm_path: Path to quantum program/circuit file.

    Returns:
        dict(str): Analyzer reports.
    """
    circuit = QuantumCircuit.from_qasm_file(qasm_path)

    circuit_bqskit_qsearch_swap_u3 = transpile(
        circuit,
        unitary_synthesis_method="qsearch",
        unitary_synthesis_plugin_config={
            "layer_generator": SimpleLayerGenerator(
                two_qudit_gate=ISwapGate(), single_qudit_gate_1=U3Gate()
            )
        },
    )

    circuit_bqskit_qsearch_greedy = transpile(
        circuit,
        unitary_synthesis_method="qsearch",
        unitary_synthesis_plugin_config={"heuristic_function": GreedyHeuristic()},
    )

    circuit_bqskit_qsearch_dijkstra = transpile(
        circuit,
        unitary_synthesis_method="qsearch",
        unitary_synthesis_plugin_config={"heuristic_function": DijkstraHeuristic()},
    )

    return {
        "bqskit_qsearch": analyze(
            transpile(circuit, unitary_synthesis_method="qsearch")
        ),
        "bqskit_qsearch_swap_u3": analyze(circuit_bqskit_qsearch_swap_u3),
        "bqskit_qsearch_greedy": analyze(circuit_bqskit_qsearch_greedy),
        "bqskit_qsearch_dijkstra": analyze(circuit_bqskit_qsearch_dijkstra),
        "bqskit_qfast": analyze(transpile(circuit, unitary_synthesis_method="qfast")),
    }


def do_benchmark(qasm_file):
    """
    Run benchmark using QASM program as an input.

    Args:
        qasm_file: Quantum program/circuit file.

    Returns:
        dict(str): Analyzer reports for different frameworks.
    """
    reports = {}
    qasm_path = path.join(QASM_DIR, qasm_file)

    reports.update(_do_benchmark_qiskit(qasm_path))
    reports.update(_do_benchmark_cirq(qasm_path))
    reports.update(_do_benchmark_pytket(qasm_path))
    reports.update(_do_benchmark_bqskit(qasm_path))

    return reports


def main():
    """
    Benchmarks entry point. Loop over QASM files and produce report.

    Returns:
        dict(str): Analyzer reports for different frameworks.
    """
    # Have to manually register this converter
    from arline_quantum.gate_chain.converters import PytketGateChainConverter

    GateChain.register_converter(PytketGateChainConverter)

    reports = {}
    for qasm_file in QASM_FILES:
        reports[qasm_file] = do_benchmark(qasm_file)

    print(json.dumps(reports))


if __name__ == "__main__":
    main()
