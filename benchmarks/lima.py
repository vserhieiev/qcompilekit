"""
Benchmarks runner.
Calculates cost function using fake Qiskit device.
Compares Qiskit and BQSKit results.

Inspired by https://github.com/mtreinish/bqskit-qiskit-synthesis-plugin
"""
import json

from arline_quantum.gate_chain.gate_chain import GateChain
from qiskit import QuantumCircuit, transpile
from qiskit.circuit.random import random_circuit
from qiskit.quantum_info.random import random_unitary

# https://qiskit.org/documentation/apidoc/providers_fake_provider.html
from qiskit.providers.fake_provider import FakeLima  #  5 qubit

from qcompilekit.analyzer import BasicAnalyser


def analyze(circuit, compiler="qiskit", target=None):
    """
    Analyze circuits from different compilers.

    Args:
        circuit: Quantum program/circuit.
        compiler: Framework (qiskit, cirq, pytket).

    Returns:
        str: Analyzer report.
    """
    # breakpoint()
    # return {
    #     "Gates count": circuit.count_ops(),
    #     "Depth": circuit.depth(),
    # }
    analyzer = BasicAnalyser()
    # return analyzer.cost_model.calculate_cost(GateChain.convert_from(circuit, compiler))
    return analyzer.run_all(
        target=target, gate_chain=GateChain.convert_from(circuit, compiler)
    )


def main():
    """
    Benchmarks entry point. Loop over random unitaries and produce report.

    Returns:
        dict(str): Analyzer reports for different frameworks.
    """
    reports = {}
    basis_gates = ["id", "u3", "cx"]
    backend = None  # FakeLima()

    unitary = random_unitary(8)
    circuit = QuantumCircuit(3)
    circuit.unitary(unitary, [0, 1, 2])
    # circuit = random_circuit(3, 100)

    reports.update(
        {
            # "qiskit": analyze(circuit),
            "qiskit_optimized1": analyze(
                transpile(circuit, backend, basis_gates, optimization_level=1)
            ),
            "qiskit_optimized2": analyze(
                transpile(circuit, backend, basis_gates, optimization_level=2)
            ),
            "qiskit_optimized3": analyze(
                transpile(circuit, backend, basis_gates, optimization_level=3)
            ),
            "bqskit_qsearch": analyze(
                transpile(
                    circuit, backend, basis_gates, unitary_synthesis_method="qsearch"
                )
            ),
            "bqskit_qfast": analyze(
                transpile(
                    circuit, backend, basis_gates, unitary_synthesis_method="qfast"
                )
            ),
        }
    )

    print(json.dumps(reports))


if __name__ == "__main__":
    main()
