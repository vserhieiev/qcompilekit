# Q Compile Kit

Plugable extention for Qiskit framework to use Berkeley Quantum Synthesis Toolkit
(BQSKit) framework for compilation (https://github.com/BQSKit/bqskit).


## Installation

QCompileKit is available for Python 3.8+ on Linux, macOS, and Windows.

```sh
pip install qcompilekit
```

Also Visual Studio Code Dev Containers configuration is included to ease
development setup.


## Basic Usage

A standard workflow utilizing QCompileKit consists of specifying a plugin when
using the Qiskit's `transpile()` function.

Search-based Synthesis example:

```python
    circuit = QuantumCircuit(...)
    ...
    result = transpile(circuit, unitary_synthesis_method ="qsearch")
```

Searchless Synthesis example:

```python
    circuit = QuantumCircuit(...)
    ...
    result = transpile(circuit, unitary_synthesis_method ="qfast")
```

Passing arguments to BQSKit `QSearchSynthesisPass`:

```python
    circuit = QuantumCircuit(...)
    ...
    config = {
        "success_threshold": 1e-1,
    }
    result = transpile(
        circuits=circuit,
        unitary_synthesis_method="qsearch",
        unitary_synthesis_plugin_config=config,
    )
```


## Benchmarks

Runs analyzer for circuits compilation/transpilation using Qiskit, Cirq, pytket
and BQSKit libraries. Benchmarks returns JSON-formatted report.

```sh
make benchmark
```



## Tests

Test suite is built using BQSKit unit tests and checks consistency of transpilation.

```sh
make test
```
