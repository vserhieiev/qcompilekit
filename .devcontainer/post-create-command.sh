#!/bin/bash
set -e

sudo apt update
sudo apt install python-is-python3 python3-pip -y
python3 -m pip install --upgrade pip setuptools
# python3 -m pip install -r requirements.txt

cd /workspaces/compiler/