"""The Q Compiler Framework Setup File."""
from __future__ import annotations

import os

from setuptools import find_packages, setup

root_dir_path = os.path.abspath(os.path.dirname(__file__))
pkg_dir_path = os.path.join(root_dir_path, "qcompilekit")
readme_path = os.path.join(root_dir_path, "README.md")
version_path = os.path.join(pkg_dir_path, "version.py")

# Load Version Number
with open(version_path) as version_file:
    exec(version_file.read())

# Load Readme
with open(readme_path) as readme_file:
    long_description = readme_file.read()

setup(
    name="qcompilekit",
    version=__version__,  # type: ignore # noqa # Defined in version.py loaded above
    description="Q Compiler Toolkit",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/vserhieiev/qcompilekit",
    author="Volodymyr Serhieiev",
    author_email="vova.sergeyev@gmail.com",
    license="BSD 3-Clause License",
    license_files=["LICENSE"],
    classifiers=[
        "Development Status :: 5 - Production/Stable",
        "Environment :: Console",
        "Intended Audience :: Developers",
        "Intended Audience :: Education",
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: BSD License",
        "Operating System :: MacOS",
        "Operating System :: Microsoft :: Windows",
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python :: 3 :: Only",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
        "Programming Language :: Python :: 3.10",
        "Programming Language :: Python :: 3.11",
        "Topic :: Scientific/Engineering",
        "Topic :: Scientific/Engineering :: Mathematics",
        "Topic :: Scientific/Engineering :: Physics",
        "Topic :: Software Development :: Compilers",
        "Typing :: Typed",
    ],
    keywords=[
        "quantum",
        "synthesis",
        "toolkit",
    ],
    project_urls={
        "Source Code": "https://gitlab.com/vserhieiev/qcompilekit",
    },
    packages=find_packages(exclude=["examples*", "test*"]),
    install_requires=[
        "arline_quantum @ https://github.com/vsergeyev/arline_quantum/tarball/master#egg=arline_quantum-0.1.8",
        "bqskit==1.0.4",
        "jkq.qcec==1.10.5",
    ],
    python_requires=">=3.8, <4",
    entry_points={
        "qiskit.unitary_synthesis": [
            "qsearch = qcompilekit.qsearch:BQSKitQSearchSynthesis",
            "qfast = qcompilekit.qfast:BQSKitQFastSynthesis",
        ]
    },
    extras_require={
        "dev": [
            "mypy",
            "pytest",
        ],
    },
)
